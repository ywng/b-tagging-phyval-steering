import os
# Author: Yvonne Ng ; Email: yvonne.ng@desy.de; 2023-4-1
##----------Configuration-----#
sample_steer="./protosteer.sh"

folder_name="Rel22_DD-MM-YY"
athena_version="22.0.32"


taskA={"name": "taskA",
       "processes": ["Zprime", "ttbar"],
       "refname": "ref2", 
       "testname": "test3", 
       "scope_tag1": "valid1", 
       "reco_tag1": "e8514_s4057_s3993_r14327_p5458_p5459_p5460", 
       "scope_tag2": "valid1", 
       "reco_tag2": "e8514_s4100_s4101_r14327_p5458_p5459_p5460"
	}

taskB={"name": "taskB",
       "processes": ["Zprime", "ttbar"],
       "refname": "ref2", 
       "testname": "test3", 
       "scope_tag1": "valid1", 
       "reco_tag1": "e8472_s3992_s3993_r14175_p5420_p5421_p5422", 
       "scope_tag2": "valid1", 
       "reco_tag2": "e8472_s3992_s3993_r14189_p5420_p5421_p5422"
	}

tasks_list=[taskA, taskB] #, taskC}

##-----------End of Configuration --------#

subtasks_list=[ task["name"]+"_"+process  for task in tasks_list  for process in task["processes"]]

#--- Making the steering script

subtask_dir_list=[]
working_dir=os.getcwd()
print("working dir: ", working_dir)

with open("overall_steer.sh",'w') as f_overall:
	f_overall.write("#!/bin/bash \n")
	for task in tasks_list:
		for process in task["processes"]: 
			#----Making directory
			directory="%s/%s_%s"%(folder_name, task["name"], process)
			print("directory: %s"%(directory))
			try:
				os.system("mkdir -p %s"%(directory))
				subtask_dir_list.append(directory)
			except Exception as e:
				print("Fail to create folder %s"%directory)
				print(e)
			steer_file='%s/steer_%s_%s.sh'%(directory, task["name"], process)
			print("steer_file: ", steer_file)
			#with open(sample_steer,'r') as f:
			f_overall.write("cd %s \n "%(directory))
			f_overall.write("source steer_%s_%s.sh | tee run_steer.log \n"%(task["name"], process))
			f_overall.write("cd %s \n"%(working_dir))

			with open(sample_steer,'r') as f:
				with open(steer_file,'w') as f2: 
					
					f2.write("#!/bin/bash \n\n")

					f2.write("#----Configurations-----\n")
					f2.write("run_date=%s \n\n"%folder_name)
					f2.write("athena_version=\"%s\"\n\n"%athena_version)
					f2.write("task_name=%s\n\n"%task["name"])
					f2.write("sample=%s\n\n"%process)
					f2.write("file1=%s \n"%task["refname"])
					f2.write("scope_tag1=%s\n"%task["scope_tag1"])
					f2.write("reco_tag1=%s\n\n"%task["reco_tag1"])

					f2.write("file2=%s\n"%task["testname"])
					f2.write("scope_tag2=%s\n"%task["scope_tag2"])
					f2.write("reco_tag2=%s\n\n"%task["reco_tag2"])
					f2.write("#----End of Configurations-----\n")
		
					f2.writelines(f.readlines()[21:])
			f2.close()
