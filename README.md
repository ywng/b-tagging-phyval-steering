# Btagging physical validation - Automation script


This script takes in basic configuration of FTAG physval tasks and automate the physics validation workflow.


Author: Yvonne Ng 

Email: yvonne.ng@DESY.de

Created: 2023-4-4



## Installation

```
git clone ssh://git@gitlab.cern.ch:7999/ywng/b-tagging-phyval-steering.git
git submodule update --init --recursive
```

## Setup environment

Change USERNAME to fit your rucio username in setup.sh
```
source setup.sh 

```


#  Example1 : Running multiple tasks_

1. Using setup-job.py to set up multiple steer.sh in 

In setup job, change the configuration to include multiple validation tasks, the following is an example: 
```
##----------Configuration-----#
sample_steer="./protosteer.sh"

folder_name="Rel22_DD-MM-YY"
athena_version="22.0.32"


taskA={"name": "taskA",
       "processes": ["Zprime", "ttbar"],
       "refname": "ref2",
       "testname": "test3",
       "scope_tag1": "valid1",
       "reco_tag1": "e8514_s4057_s3993_r14327_p5458_p5459_p5460",
       "scope_tag2": "valid1",
       "reco_tag2": "e8514_s4100_s4101_r14327_p5458_p5459_p5460"
    }

taskB={"name": "taskB",
       "processes": ["Zprime", "ttbar"],
       "refname": "ref2",
       "testname": "test3",
       "scope_tag1": "valid1",
       "reco_tag1": "e8472_s3992_s3993_r14175_p5420_p5421_p5422",
       "scope_tag2": "valid1",
       "reco_tag2": "e8472_s3992_s3993_r14189_p5420_p5421_p5422"
    }

tasks_list=[taskA, taskB]

##-----------End of Configuration --------#
```

Run: 
```
python setup-job.py
```
Following this, multiple steer files should be gerenerated:

```
directory: Rel22_DD-MM-YY/taskA_Zprime
steer_file:  Rel22_DD-MM-YY/taskA_Zprime/steer_taskA_Zprime.sh
directory: Rel22_DD-MM-YY/taskA_ttbar
steer_file:  Rel22_DD-MM-YY/taskA_ttbar/steer_taskA_ttbar.sh
directory: Rel22_DD-MM-YY/taskB_Zprime
steer_file:  Rel22_DD-MM-YY/taskB_Zprime/steer_taskB_Zprime.sh
directory: Rel22_DD-MM-YY/taskB_ttbar
steer_file:  Rel22_DD-MM-YY/taskB_ttbar/steer_taskB_ttbar.sh
```

An "overall_steer.py" file is created, which runs the above steer.sh files in sequential order. 

2. Running the overall_steer.sh file. 
```
source overall_steer.sh
```
A log file is created in the respective subtask folder to check for errors. 

3. Move everything in bulk to the physval web space:
```
python bulk_move.py --folder Rel_XX_XX_XX 
```

All files should then be sent to the desingated space to generate webplot. 


# Example1: Running a single task
 Edit the *protosteer.sh* script. Fill in the correct Rel name/tag/container etc.
```
run_date=Rel22_protosteer

task_name=task1B
sample=Zprime

file1="ref2"
scope_tag1="valid1"
reco_tag1=s3822_r13559_p5061_p5062_p5063

file2="test"
scope_tag2="valid1"
reco_tag2=s3822_r13560_p5061_p5062_p5063
```

Now to run the *protosteer.sh* script:
```
mkdir task_toy
cp protosteer.sh task_toy
cd task_toy
source protosteer.sh
```

Protosteer is split into steps, can comment/modify steps as needed, the default follows a typical run down. 
```
Run_setupAthena $athena_version
#Run_setupRucio
(Run_makesubdir)
(Run_cprunscript)
(Run_ruciodownload1)
(Run_ruciodownload2)
(Run_mergefiles)
(Run_makeplots)
(Run_makeROC)
#(Run_cptoweb)
```

