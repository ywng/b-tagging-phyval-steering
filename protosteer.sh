#!/bin/bash
#####################################################
# Author: Yvonne Ng;  Email: yvonne.ng@desy.de 2023-4-1
#TODO make function run command
#TODO print out error messages
#TODO turn these all to athena scripts

run_date=Rel22_XXXXXXXXXX

task_name=taskXX
# Zprime or ttbar or EZprime
sample=XXXXXXXX

file1="ref"
scope_tag1="valid1"
reco_tag1=XXXXXXX

file2="test"
scope_tag2="valid1"
reco_tag2=XXXXXX

athena_version="22.0.32"
# ----Do not chnage below line 21 -----
if [ $sample == "ttbar" ]
    then DSID=601229
        echo "DSID set for ttbar"
elif [ $sample == "Zprime" ]
    then
        echo "DSID set for Zprime"
        DSID=801271
elif [ $sample == "EZprime" ]
    then
        echo "DSID set for Zprime"
        DSID=800030
fi


echo "===Initial Config==="
echo "run_date: " $run_date
echo "task_name: " $task_name
echo "sample: " $sample
echo "DSID: " $DSID
echo "scope_tag1: " $scope_tag1
echo "reco_tag1" $reco_tag1
echo "file1: " $file1
echo "scope_tag2: " $scope_tag2
echo "reco_tag2" $reco_tag2
echo "file2: " $file2
echo "======================"
echo
sleep 5
#make a log file

source $ROOTDIR/lib/protolib.sh

echo athena version $athena_version

Run_setupAthena $athena_version
###Run_setupRucio
(Run_makesubdir)
(Run_cprunscript)
(Run_ruciodownload1)
(Run_ruciodownload2)
(Run_mergefiles)
(Run_makeplots)
(Run_makeROC)
#(Run_cptoweb)

